# ReproducibleAnalysisRGit
Reproducible Analysis with R and Git: This is a tutorial to start with reproducible analysis using R and Git. 


---


# Contents
**The [ReproducibleAnalysisRGit.html](ReproducibleAnalysisRGit.html) document is the tutorial. Save it and open it in your browser because Codeberg does not do well with displaying HTML files.**  
To download an HTML file from Codeberg, first display the “raw” file and then save it as HTML.

Alternatively, the [ReproducibleAnalysisRGit.md](ReproducibleAnalysisRGit.md) file can be displayed online, but some features are not available (automatic numbering, floating TOC...). 

The other files are not meant to be used for the tutorial itself but rather for improving the tutorial only: 

* The [ReproducibleAnalysisRGit.Rmd](ReproducibleAnalysisRGit.Rmd) file contains the source code of the HTML document.  
* The [ReproducibleAnalysisRGit.Rproj](ReproducibleAnalysisRGit.Rproj) file is the RStudio project file.

The [figs](figs) directory contains images used in the script.


---


# How to contribute
I appreciate any comment from anyone (expert or novice) to improve this document, so do not be shy! There are three possibilities to contribute.

1. Submit an issue: If you notice any problem or have a question, submit an [issue](https://docs.codeberg.org/getting-started/issue-tracking-basics/). You can do so [here](https://codeberg.org/ivan-paleo/ReproducibleAnalysisRGit/issues).  
A Codeberg account is necessary. 

2. Propose changes: This document is written in [Rmarkdown](https://bookdown.org/yihui/rmarkdown/). If you know how to write in this format, please propose text edits as a [pull request](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/) (abbreviated "PR", see also [documentation on GitHub](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests)).  
A Codeberg account is necessary. 

3. Send me an email: If you do not have a Codeberg account and do not want to sign up, you can still write me an email (Google my name together with my affiliation to find my email address).

By participating in this project, you agree to abide by our [code of conduct](CONDUCT.md).


---


# License
[![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

See also [License file](LICENSE) in the repository.

Author: Ivan Calandra

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg


---


# References
Soler S. 2022.cc-licenses: Creative Commons Licenses for GitHub Projects. Available at https://github.com/santisoler/cc-licenses (accessed September 27, 2022)

See also file [ReproducibleAnalysisRGit.bib](ReproducibleAnalysisRGit.bib) for a bibliography (created with the package [grateful](https://github.com/Pakillo/grateful)).