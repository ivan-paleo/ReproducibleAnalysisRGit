Reproducible analysis with R and Git
================
Ivan Calandra
2023-08-31 17:36:56 CEST

- [Knitr options](#knitr-options)
- [Overview and goals](#overview-and-goals)
- [Research Compendia](#research-compendia)
  - [Project root](#project-root)
  - [Raw *vs.* derived data](#raw-vs-derived-data)
  - [Scripts](#scripts)
  - [Output](#output)
  - [Package renv](#package-renv)
  - [Rendering with R Markdown](#rendering-with-r-markdown)
  - [Creating research compendia](#creating-research-compendia)
- [Git and RStudio](#git-and-rstudio)
  - [Git](#git)
  - [GitHub, GitLab and Codeberg](#github-gitlab-and-codeberg)
  - [Linking to RStudio](#linking-to-rstudio)
  - [Using Git and GitHub-GitLab-Codeberg from
    RStudio](#using-git-and-github-gitlab-codeberg-from-rstudio)
- [Summary and further readings](#summary-and-further-readings)
- [sessionInfo()](#sessioninfo)
- [Cite R packages used](#cite-r-packages-used)
  - [References](#references)

# Knitr options

``` r
knitr::opts_chunk$set(comment = NA, message = FALSE, indent = "", error = TRUE, eval = FALSE)
```

------------------------------------------------------------------------

# Overview and goals

**This tutorial tries to provide some ideas on how to improve
repeatability and reproducibility when doing statistical analyses.** It
explains the basics of using R (and some specific packages), RStudio,
Git and GitHub/GitLab/Codeberg. Using these tools is an important step
for repeatability and reproducibility in a transparent and explicit way,
but there are other complementary ways to make your project fully
repeatable and reproducible.

This tutorial has three main goals:

1.  Explain what a research compendium is and how it should be used to
    organized your data.  
2.  Explain how to make the analysis reproducible through the creation
    of a research compendium (using the package `rrtools`), rendering
    with R Markdown, and saving the R computing environment with the
    package `renv`.  
3.  Give an overview of Git and GitHub/GitLab/Codeberg and of how to use
    them through RStudio.

**This tutorial is not intended as an exhaustive reference**, but it
should get you started. Feel free to ask me if you have more questions.

------------------------------------------------------------------------

# Research Compendia

Here, I follow [Marwick et
al. (2018)](https://www.tandfonline.com/doi/full/10.1080/00031305.2017.1375986)’s
idea of a research compendium to organize analysis data.

The goal of **a research compendium is to collect and collate all data
and output related to a given analysis into a structured project**. This
makes the files easier to identify and find, ultimately facilitating the
sharing of, and the collaboration on, these files. All this serves to
**increase repeatability and reproducibility**.

Marwick et al. (2018) use the structure of an [R
package](https://r-pkgs.org/) for their research compendia. But this
could be adapted to other types of analysis using any software.

Here, I also use [R](https://www.r-project.org/). The structure of my
research compendia is based on Marwick et al. (2018)’s figure 2, but
slightly modified to fit my (our?) needs. My organization is:

- In the main folder (= **project root**), I have 6 important files:
  - `CONDUCT.md`  
  - `CONTRIBUTING.md`  
  - `DESCRIPTION`  
  - `LICENSE.md` and/or, depending on the license type, `LICENSE` (see
    section [Creating research compendia](#creating-research-compendia))
  - `README.md` (based on `README.Rmd`)  
  - the R project’s file (`*.Rproj`)  
- In the project root, I have a folder `analysis`, which contains 5
  folders:
  - `derived_data`  
  - `plots`  
  - `raw_data`  
  - `scripts`  
  - `summary_stats`  
- I have another folder in the project root called `renv` which,
  together with the `renv.lock` file in the project root, are related to
  the package `renv` (see section [Package renv](#package-renv)).

## Project root

The `README.md` file is very important. It is where you explain what the
project (“repository” in GitHub//GitLab/Codeberg language, see section
[GitHub, GitLab and Codeberg](#github-gitlab-and-codeberg)) is for and
how it is organized. On GitHub/GitLab/Codeberg, it is the first file to
be displayed; in other words, this is where every “user” of your project
will start exploring it. This is why it should be some sort of guide on
how to use your project.  
This file is created by rendering the `README.Rmd` file (see section
[Rendering with R Markdown](#rendering-with-r-markdown)).

The `LICENSE.md` file makes it explicit under which terms your project
can be used. There are several types of license that could be relevant.
Ben Marwick recommends to publish text and figures under
[CC-BY-4.0](http://creativecommons.org/licenses/by/4.0/), the code under
[MIT](https://opensource.org/licenses/MIT) and the data under
[CC-0](http://creativecommons.org/publicdomain/zero/1.0/) licenses, but
his recommendation is for the USA. Still, it is probably relevant for
Europe/Germany as well (but ask a lawyer if you want to be sure!).  
I have chosen the [CC BY-NC-SA
4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license for
this tutorial (see
[License](https://codeberg.org/ivan-paleo/ReproducibleAnalysisRGit/src/branch/main/LICENSE)).  
The [Free Software Foundation](https://www.fsf.org/) also provides some
[guidelines](https://www.gnu.org/licenses/license-recommendations.html)
on licensing aspects on the (related) GNU website.  
Whatever license you choose, it becomes explicit to the user which
licenses apply so that he/she knows what he/she can, and cannot, do with
your data.

The `DESCRIPTION` file “provides formally structured, machine- and
human-readable information about the authors, the project license, the
software dependencies, and other metadata of the compendium. \[…\] R’s
built-in `citation()` function can use that metadata, along with
references to any publications that result from the project, to provide
users with the necessary information to cite your work.” (Marwick et
al. 2018, p. 83).

The `CONDUCT.md` file describes the code of conduct for contributors,
*i.e*. rules that every person willing to contribute to this project
should follow. This is very important in case you want to share your
project with the expectations that other people will help you develop
this project further. This is less important if you use the research
compendium only as an archive of your work. But it cannot hurt, since
anyone could potentially contribute in the future.

The `CONTRIBUTING.md` file explains how people willing to contribute
should do it so that suggestions and improvements can be made and
implemented as easily as possible. This relate to the functioning of
GitHub (see section [GitHub, GitLab and
Codeberg](#github-gitlab-and-codeberg)).

The R project’s file (`*.Rproj`) is a file that is automatically created
and edited by RStudio when you create a project, which is recommended
(see section “Data, scripting, projects and repeatability” of my
[initiationR](https://codeberg.org/ivan-paleo/initiationR) tutorial or
this [blog
entry](https://www.tidyverse.org/blog/2017/12/workflow-vs-script/)).

## Raw *vs.* derived data

According to Marwick et al. (2018), it is important to separate **raw
data** from **derived data**. Although I had never realized this before
reading it, I now fully agree with them.

Raw data are the data that come directly out of your machine, without
any processing. Derived data, on the other hand, are processed data.
This processing should of course be clearly documented for repeatability
and reproducibility.

For example, when I acquire 3D surface data with the LSM800, I analyze
them through templates with ConfoMap/MountainsMap and the output is a
CSV file containing a table of ISO 25178 parameter values for every
surface. This CSV file constitutes the **raw data** (= input) **of my
statistical R analysis**. But this CSV file is in an unfriendly form so
it needs reformatting (editing headers, extracting units,
renaming/splitting columns…), which I perform with a script in R. I then
export the formatted data table to XLSX and Rbin files, which are
therefore **derived data** files.  
In the strict sense, plots, summary stats, maps or whatever are also
derived data, but I choose to separate them from each other.

As far as possible, you should use open formats for all data files.
Regarding raw and derived data, most of them are in tabular forms. Four
formats are relevant here:

- [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) is
  probably the best choice for openness, but it has limitations. What I
  often lack with this format is the possibility to have several sheets
  (I usually have one for the data themselves, and one for the units of
  the parameters).  
- [XLS](https://en.wikipedia.org/wiki/Microsoft_Excel#File_formats) is a
  Microsoft Excel proprietary format. Avoid it as much as possible!  
- XLSX, part of the [Open Office
  XML](https://en.wikipedia.org/wiki/Office_Open_XML) format, is an open
  format developed for MS Excel. But it has compatibility issues with
  other software, like LibreOffice, because [it is not a fully open
  format](https://brattahlid.wordpress.com/2012/05/08/is-docx-really-an-open-standard/).  
- [ODS](https://en.wikipedia.org/wiki/OpenDocument) is a fully open
  format, mainly used by open source software like
  [LibreOffice](https://www.libreoffice.org/). The package
  [readODS](https://github.com/chainsawriot/readODS) can both read and
  write ODS files. Until quite recently, it did not allow the writing of
  ODS files with several sheets but it is now fine (see issue
  [\#56](https://github.com/chainsawriot/readODS/issues/56)). It is
  unfortunately less efficient than
  e.g. [readxl](https://readxl.tidyverse.org/),
  [writexl](https://docs.ropensci.org/writexl/) or
  [openxlsx2](https://github.com/JanMarvin/openxlsx2) for XLSX files,
  although it seems to have improved dramatically recently (see issues
  [\#49](https://github.com/chainsawriot/readODS/issues/49) and
  [\#71](https://github.com/chainsawriot/readODS/issues/71)); it might
  therefore be problematic for large files. Even though it is fully
  open, ODS format is still not as widely used as CSV, probably
  explaining why there is only one R package to deal with such files.

Currently, I still often use the XLSX format for the derived data, even
though it is not a fully open format, but I have started to move away
from XLSX files and use ODS files instead. Most raw data are in CSV
format when exported from the software packages (ConfoMap/MountainsMap,
ZEN core/blue…).

## Scripts

Scripts are of course the core of a repeatable and reproducible
analysis.  
**A script, in that sense, is a set of commands that can be run again
and again with the exact same workflow.**  
In R, the scripts have the extension `*.R`.

Even if the script gives clear instructions on how to perform the
analysis, the output of this analysis still depends on the data on which
the commands have been run.  
Several complementary steps can be undertaken to improve it:

- Sharing the scripts together with the raw data.  
- Adding a time stamp, or even better a
  [checksum](https://en.wikipedia.org/wiki/Checksum) (e.g. [MD5
  hashes](https://en.wikipedia.org/wiki/MD5) using the R function
  `md5sum()`), to ensure that the raw data uploaded is the same as the
  raw data used.  
- Using [R Markdown](https://rmarkdown.rstudio.com/) to render your
  analysis into a document that includes code, output and explanatory
  text. With this, you can incorporate the checksum of the raw data file
  in the resulting document, explicitly showing which raw data file you
  used to create the output. In this case, the scripts are R Markdown
  files with the extension `*.Rmd`. See section 3.6 [Rendering with R
  Markdown](#rendering-with-r-markdown) for details.

Another issue is that in R, since most commands are functions from
packages, different versions of R and of these packages might produce
different outputs. We will come back to this issue in the section
[Package renv](#package-renv).

## Output

Here again, **open formats should be preferred**.

Regarding plots, most of the time, you will want to [use vector graphics
rather than raster
images](https://www.geeksforgeeks.org/vector-vs-raster-graphics/).  
R offers many possibilities to save plots as **vector graphics**. My
favorite on Windows is the PDF format (using the PDF
[device](https://bookdown.org/rdpeng/exdata/graphics-devices.html)).
[PDF](https://en.wikipedia.org/wiki/PDF) is a widely used open format
that supports vector graphics and that can easily be edited in any
vector graphic software, such as [Inkscape](https://inkscape.org/),
[Affinity Designer](https://affinity.serif.com/en-gb/designer/) or
[Adobe Illustrator](https://www.adobe.com/products/illustrator.html)
(see section “Working with vector graphics” of my
[initiationR](https://codeberg.org/ivan-paleo/initiationR) tutorial for
details).

## Package renv

The package [renv](https://rstudio.github.io/renv/index.html) “helps you
create reproducible environments for your R projects”.  
**`renv` saves the list of packages used in your project, as well as
their versions, together with your project.** In practice, it does two
things:

- It isolates your project: every package installation, removal or
  updating will stay within the project. In other words, it has no
  influence on your other projects or on your R installation.  
- It saves the list of packages and their versions in the `renv.lock`
  file. When someone (or you on a new computer or R installation) opens
  the project, `renv` will check and install the right packages within
  the project, so that this person (or you) can have the same computing
  environment as you did when you produced the data for your project. Of
  course, it cannot install the correct R version, but it will warn if
  the project was created with a version different from the one you
  used.

Thanks to this, **reproducibility is improved**. It also has the nice
side-effect of **improving portability**, *i.e.* the ability to transfer
the project to another computer without changing the computing
environment.

Marwick et al. (2018) recommend using [Docker](https://www.docker.com/)
because it saves more than just the R computing environment: it saves
the whole computing environment (including versions of OS, R and
whatever background software). This is unfortunately much more involved
and I am not there yet; I would need someone to show it to me!  
An interesting alternative is [Binder](https://mybinder.org/), which
builds a Docker file of a GitHub repository and then creates an
interactive online session to explore the repository.

## Rendering with R Markdown

**When you render an R Markdown script (`*.Rmd`), the plain text will be
nicely displayed according to the theme you choose, the R code will be
run and the output will be shown in the output document** (if you want
that to happen, of course). This is like running a script, except that
it **incorporates the raw data, the output and your comments in a single
file.**

To go from your R Markdown script to your output document, a process
called **rendering**, you can either use the function `render()` from
the package `rmarkdown`, or simply click on the `Knit` button if you use
RStudio (or use the keyboard shortcut `CTRL+SHIFT+k`).  
In the background, your R Markdown document will be converted to a
Markdown file using the package `knitr`, which will in turn be converted
to the desired output format using [Pandoc](https://pandoc.org/).

------------------------------------------------------------------------

> ![](figs/rmarkdownflow.png)

*R Markdown workflow
([source](https://rmarkdown.rstudio.com/lesson-2.html))*

------------------------------------------------------------------------

For details on how that works and for a guide on how to do it, check the
[R Markdown for RStudio
website](https://rmarkdown.rstudio.com/lesson-1.html).

Many output formats are available. Four are relevant here:

- PDF (see section [Output](#output))  
- [DOCX](https://en.wikipedia.org/wiki/Office_Open_XML)  
- [HTML](https://en.wikipedia.org/wiki/HTML)  
- [Mardown](https://en.wikipedia.org/wiki/Markdown), either the
  intermediate step from `knitr` or a slightly different output format
  from Pandoc

I like the HTML output very much because it is very flexible and allows
floating table of contents, automatic section numbering and so on (like
this document). Unfortunately, it is not well displayed in
GitHub/GitLab/Codeberg. For these platform, prefer the Markdown format
(extension `*.md`); there is even a [“flavour” of Markdown for
GitHub](https://github.github.com/gfm/).  
You can find more information
[here](https://happygitwithr.com/rmd-test-drive.html) but make sure you
read the section [Git and RStudio](#git-and-rstudio) first.

R itself is not well equipped to deal with Markdown; it can, but it is
not user-friendly (arguable, R is not user-friendly for anything!). To
do so, it is recommended to use [RStudio](https://rstudio.com/).  
Note that it is possible to [convert an Rmd script into an R
script](https://happygitwithr.com/r-test-drive.html). I am not sure
whether it really is useful, but it is possible. The only advantages are
that you do not need RStudio (is it an advantage?) and that you need to
type less if you have a lot of code but not so much text (but the
difference seems minimal).  
Also note that there are other nice features available in RStudio:
syntax highlighting, code completion, projects, connecting to Git (see
section [Linking to RStudio](#linking-to-rstudio))…

## Creating research compendia

The final step of this section is about the creation of such a research
compendium!  
This is done with the package `rrtools`. For details on `rrtools`, and
especially on how to install and use it, see Ben Marwick’s [GitHub
repository](https://github.com/benmarwick/rrtools).  
Note that the code below is not evaluated (not run) during the rendering
(`knitr::opts_chunk$set(eval=FALSE)` in the section [Knitr
options](#knitr-options)).

1.  Make sure that the packages `usethis`, `rrtools` and `renv` are
    installed.

2.  Ideally, create a GitHub/GitLab/Codeberg repository first and clone
    the repository to an RStudio project (see section [Linking to
    RStudio](#linking-to-rstudio))

3.  Create the compendium:

``` r
rrtools::use_compendium()
```

When you run this command, a new project should open in a new session.  
You need to edit the fields “Title”, “Author” and “Description” in the
`DESCRIPTION` file.

4.  Create the license file using **one** of these commands:

``` r
usethis::use_ccby_license()
usethis::use_cc0_license()
usethis::use_mit_license(copyright_holder = "your name")
```

Unfortunately, it is not possible to have all three within one license
file.  
Note that with the MIT license, two files will be created: `LICENSE`
(year and copyright holder) and `LICENSE.md` (details of the license,
including year and copyright holder). Arguably, the first file is
useless, but it has to be there apparently.

5.  Create the `README.Rmd` file:

``` r
rrtools::use_readme_rmd()
```

Edit the `README.Rmd` file as needed.  
Make sure you render (knit) it to create the `README.md` file, which
will be the “home page” of your GitHub repository.

6.  Create the necessary folders (adapt as needed):

``` r
dir.create("analysis", showWarnings = FALSE)
dir.create("analysis/raw_data", showWarnings = FALSE)
dir.create("analysis/derived_data", showWarnings = FALSE)
dir.create("analysis/plots", showWarnings = FALSE)
dir.create("analysis/summary_stats", showWarnings = FALSE)
dir.create("analysis/scripts", showWarnings = FALSE)
```

Note that the folders cannot be pushed to GitHub/GitLab/Codeberg as long
as they are empty (*i.e.* contain no file).

7.  Delete folder file `NAMESPACE`:

``` r
file.remove("NAMESPACE")
```

8.  After the creation of the research compendium but before running the
    analyses, move the raw data files to `"~/analysis/raw_data"` (as
    **read-only** files) and the scripts to `"~/analysis/scripts"`.

9.  Run the analyses (*i.e.* render the R Markdown scripts).

10. After running the analyses, run this command to add the dependencies
    (*i.e.* the packages that you used in your scripts) to the
    `DESCRIPTION` file:

``` r
rrtools::add_dependencies_to_description()
```

11. Initialize `renv` to save the state of the project library:

``` r
renv::init()
```

------------------------------------------------------------------------

# Git and RStudio

This section should give you an overview of what Git and
GitHub/GitLab/Codeberg are, of how to link them with RStudio, and of how
to use these functionalities in RStudio.  
For more details, see [Happy Git and GitHub for the
useR](https://happygitwithr.com/) (*Happy Git* for short).

## Git

When you write files, whether a text document, an image or a script, you
constantly edit things, and you sometimes realize that you should not
have edited in that way. Of course you try to keep track with “v1”,
“v2”… or time stamps.  
MS Word, LibreOffice and GoogleDocs for example allow you to keep track
of these changes in documents, spreadsheet and presentations with the
“Track Changes” functions. But R/RStudio cannot do that for scripts.
This is where [Git](https://git-scm.com/) comes in. It basically records
the modifications to every file in your project, so that you can come
[back if needed](https://happygitwithr.com/time-travel-see-past.html).
This is called [version
control](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control).  
Version control becomes even more important when you are not alone
working on that project: you want to be able to know what collaborators
have changed, and to be able to accept or reject their modifications,
just like you do with a paper manuscript.

**Git is like “Track Changes” of MS Word but for any kind of file. In
our case, we need it for scripts.**

Happy Git has a section on [how to install
Git](https://happygitwithr.com/install-git.html). This is very easy [on
Windows](https://happygitwithr.com/install-git.html#install-git-windows)
if you follow the option 1. You then need to [configure
Git](https://happygitwithr.com/hello-git.html). Note that you need to
[register to GitHub first](https://happygitwithr.com/github-acct.html);
of course GitLab or Codeberg are equally valid (see below).

## GitHub, GitLab and Codeberg

[GitHub](https://github.com/), [GitLab](https://about.gitlab.com/) and
[Codeberg](https://codeberg.org/) are **like “clouds” and “interfaces”
for Git**. They all have many features, but most of them are useless to
us archeologists running some R/Python analyses! What is important for
us is that you can use GitHub/GitLab/Codeberg as an online repository to
store and share your research compendium, and that other
GitHub/GitLab/Codeberg users can collaborate with you on a project using
the version control system of Git.  
Version control here is also a way of being transparent about the
development of your analysis workflow, because everyone can see the
gradual changes.

In GitHub/GitLab/Codeberg language, a project is called a
**repository**. Repositories can be made public or private. Even if
private, you can invite GitHub/GitLab/Codeberg users to collaborate on
your repository. But latest when the paper associated to the research
compendium is published, you should make sure the repository is
public.  
Once public, every one can view and explore your repository, and any
GitHub/GitLab/Codeberg user can contribute to the repository: ask
questions, alert you about issues (errors/mistakes), suggest
improvements… This could also potentially help initiating future
collaborations.

Note that there is one drawback of using GitHub/GitLab/Codeberg as an
online repository: they do not generate DOIs for its repositories. On
the other hand, other platforms like [Zenodo](https://zenodo.org/) do
it; however, Zenodo can only host files, not folders.  
But it is possible to link GitHub and Zenodo, to make the [code
citable](https://docs.github.com/en/repositories/archiving-a-github-repository/referencing-and-citing-content).
As far as I know, this is not (yet?) possible with
[GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/18763) or
[Codeberg](https://codeberg.org/Codeberg/Community/issues/295)
repositories.  
It is also possible to upload the [ZIP
archive](https://help.github.com/en/github/creating-cloning-and-archiving-repositories/cloning-a-repository)
of the GitHub/GitLab/Codeberg repository to any online repository and
refer to the DOI in the publication.

As mentioned earlier, the “home page” of a repository will show the tree
view of the files and folders in the project root and the `README.md`
will be displayed below.  
This is for example what you see if you go to a [Codeberg repository of
mine](https://codeberg.org/ivan-paleo/initiationR):

------------------------------------------------------------------------

> ![](figs/Repository_overview.png)

*“Home page” of the repository (README truncated)*

------------------------------------------------------------------------

If you go to one of the files, for example `README.md`, you can check
its “History”. Click on one of the commits (see section[Using Git and
GitHub-GitLab-Codeberg from
RStudio](#using-git-and-github-gitlab-codeberg-from-rstudio)) to see
what changed between the versions:

------------------------------------------------------------------------

> ![](figs/History.png)

*History of modifications (red are deletions and green are additions)*

------------------------------------------------------------------------

GitHub/GitLab/Codeberg have a nice interface and look good, but at the
end of the day, I do not use them much. The whole writing (scripting)
happens in RStudio and GitHub/GitLab/Codeberg is there only to “host”
the project with its versions.

Of course, GitHub/GitLab/Codeberg does not need RStudio to work. You can
work either directly through Git (with [command
lines](https://happygitwithr.com/push-pull-github.html)), with a [Git
client](https://happygitwithr.com/git-client.html), e.g. [GitHub
Desktop](https://desktop.github.com/), or of course in the web
interfaces.

For more details, check the [Get started
guide](https://guides.github.com/activities/hello-world/) and the
[docs](https://docs.github.com/en).

------------------------------------------------------------------------

Until now, I have always grouped GitHub, GitLab and Codeberg. In
principle, they all fulfill the same “purpose”. But they do it
differently, with different focus.  
I do not like GitLab too much; I find it a bit too much
“production”-oriented. GitHub is really nice and benefits from a huge
user community.  
Unfortunately, both GitHub and GitLab are privately owned ([Microsoft
bought GitHub in
2018](https://blogs.microsoft.com/blog/2018/10/26/microsoft-completes-github-acquisition/)).
You can self-host GitLab, but this is probably not something that every
individual can/should do.  
I have discovered very recently about Codeberg (by reading [this
post](https://steko.iosa.it/2020/05/all-my-source-code-repositories-are-now-on-codeberg/)):
it is a non profit organization based in Germany, and the service is
based on the open-source software [Gitea](https://gitea.io). Almost all
functions from GitHub are available in Codeberg too. The documentation
is still in its infancy but I have already had a very nice experience
with the community help. So I can only recommend using it! I have moved
the [repositories](https://codeberg.org/ivan-paleo), on which I work
alone, to Codeberg :)

As a final note, the free user accounts (GitHub Free for user accounts,
Free accounts on GitLab, or all accounts on Codeberg) already offer
enough storage space and features.  
Additionally, they all offer the possibility to create organizations for
free (e.g. GitHub Free for organizations, see
[details](https://help.github.com/en/github/setting-up-and-managing-organizations-and-teams)).
We already have one for [TraCEr](https://github.com/tracer-monrepos) on
GitHub.  
You can check the
[features](https://help.github.com/en/github/getting-started-with-github/githubs-products)
and [prices](https://github.com/pricing) for the different options.

## Linking to RStudio

Once you have a GitHub/GitLab/Codeberg account, and installed and
configured Git, you can start using Git and GitHub/GitLab/Codeberg
functionalities directly from RStudio. Happy Git again has a nice
overview of how it works
[here](https://happygitwithr.com/rstudio-git-github.html) or
[here](https://happygitwithr.com/new-github-first.html).

It is recommended that you **start by creating a repository on
GitHub/GitLab/Codeberg** as shown
[here](https://happygitwithr.com/rstudio-git-github.html#make-a-repo-on-github-1)
or
[here](https://happygitwithr.com/new-github-first.html#make-a-repo-on-github-2).  
Then, **clone** this repository to your computer using RStudio, as shown
[here](https://happygitwithr.com/rstudio-git-github.html#clone-the-new-github-repository-to-your-computer-via-rstudio)
or
[here](https://happygitwithr.com/new-github-first.html#new-rstudio-project-via-git-clone).

From now on, you can work on your project in RStudio.

## Using Git and GitHub-GitLab-Codeberg from RStudio

You can save the modifications to one or several files at anytime, as a
so-called **commit**.  
“A commit functions like a snapshot of all the files in the
repo\[sitory\], at a specific moment” ([Happy Git, chapter
20.2](https://happygitwithr.com/git-basics.html#commits-diffs-and-tags)).  
The procedure is easy, as shown
[here](https://happygitwithr.com/rstudio-git-github.html#make-local-changes-save-commit)
or
[here](https://happygitwithr.com/new-github-first.html#make-local-changes-save-commit-1).  
Make sure that you [commit
often](https://happygitwithr.com/repeated-amend.html).

To commit, first access the version control functions in RStudio either
through the menu or with CTRL+ALT+M (on Windows).

------------------------------------------------------------------------

> ![](figs/version_control_RStudio.png)

*Access the version control functions in RStudio*

------------------------------------------------------------------------

In the new window, you can select the files you want to include in the
commit and review the changes for each of those files.

------------------------------------------------------------------------

> ![](figs/Review_changes.png)

*Review changes, pull, commit and push. Note that the commit message is
missing*

------------------------------------------------------------------------

It is easy to tick several files at once, but it is not obvious: just
select them with the mouse (using [CTRL+click,
SHIFT+click](https://www.sdmfoundation.net/2017/11/02/selecting-using-command-control-shift-make-selections/)
or [CTRL+A](https://www.computerhope.com/jargon/s/selectall.htm)) or
arrows, and press ENTER to tick all the selected files at once.

Make sure that you write a [commit
message](https://happygitwithr.com/git-basics.html#commits-diffs-and-tags);
this is a requirement. It is not easy to write a good message, but there
are
[guidelines](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)
for it out there.  
Now, click on “Commit”.

You can then **push** this commit to GitHub/GitLab/Codeberg, *i.e.*
upload your modifications, as shown
[here](https://happygitwithr.com/rstudio-git-github.html#push-your-local-changes-online-to-github).  
To download the files (or their modifications) from
GitHub/GitLab/Codeberg, you **pull** from GitHub/GitLab/Codeberg. This
is important when you work on different computers to make sure that all
computers are on the same stage. It is even more important when you have
collaborators on a project; they might change the files too, so you need
to pull these modifications before you start working.

Because of that, it is important that you **pull before you push** as
explained
[here](https://happygitwithr.com/new-github-first.html#push-your-local-changes-to-github).
This will avoid some issues.  
Even then, it has happened to me a few times that I was the only one
working of the files, on a single machine, and my push was rejected
because the online version had modifications I did not have locally. I
still have not quite understood how that is possible (although I have a
few ideas).  
There are a few pieces of advice you can follow to avoid and deal with
these issues: [here](https://happygitwithr.com/push-rejected.html) and
[here](https://happygitwithr.com/pull-tricky.html).

Of course, it is also possible to [make changes directly on
GitHub/GitLab/Codeberg](https://happygitwithr.com/new-github-first.html#make-a-change-on-github).
In that case, when you go back to RStudio, do not forget to
[pull](https://happygitwithr.com/new-github-first.html#pull-from-github)
first.

You sometimes already have a project but it is not on
GitHub/GitLab/Codeberg. In this case, you do not want to follow the [new
project, GitHub/GitLab/Codeberg
first](https://happygitwithr.com/new-github-first.html) approach as we
just did. You can follow one of these two alternative approaches:
[existing project, GitHub/GitLab/Codeberg
first](https://happygitwithr.com/existing-github-first.html) or
[existing project, GitHub/GitLab/Codeberg
last](https://happygitwithr.com/existing-github-last.html).

------------------------------------------------------------------------

# Summary and further readings

There is quite a lot of information in this document, but hopefully, you
now have a good basic knowledge of Git, GitHub/GitLab/Codeberg and
research compendia, as well as some ideas on how to improve
repeatability and reproducibility of your analysis, all that in RStudio.

None of the topics are covered exhaustively. Here are some further
readings on some topics:

- [Happy Git](https://happygitwithr.com/)  
- [R Markdown with RStudio](https://rmarkdown.rstudio.com/)  
- [R Markdown: The Definitive
  Guide](https://bookdown.org/yihui/rmarkdown/)  
- [GitHub docs](https://docs.github.com/en)  
- [Introduction to the package
  renv](https://rstudio.github.io/renv/articles/renv.html)
- Resources for the package rrtools: [GitHub
  repository](https://github.com/benmarwick/rrtools) and
  [tutorial](https://github.com/annakrystalli/rrtools-repro-research)

Some more general resources on reproducible research:

- [The Turning Way](https://the-turing-way.netlify.app/welcome.html)  
- [UK Reproducibility Network](https://www.ukrn.org/)

------------------------------------------------------------------------

# sessionInfo()

``` r
sessionInfo()
```

    R version 4.3.1 (2023-06-16 ucrt)
    Platform: x86_64-w64-mingw32/x64 (64-bit)
    Running under: Windows 10 x64 (build 19043)

    Matrix products: default


    locale:
    [1] LC_COLLATE=English_United States.utf8 
    [2] LC_CTYPE=English_United States.utf8   
    [3] LC_MONETARY=English_United States.utf8
    [4] LC_NUMERIC=C                          
    [5] LC_TIME=English_United States.utf8    

    time zone: Europe/Berlin
    tzcode source: internal

    attached base packages:
    [1] stats     graphics  grDevices utils     datasets  methods   base     

    other attached packages:
    [1] grateful_0.2.0

    loaded via a namespace (and not attached):
     [1] digest_0.6.33     R6_2.5.1          fastmap_1.1.1     xfun_0.39        
     [5] cachem_1.0.8      knitr_1.43        htmltools_0.5.5   rmarkdown_2.23   
     [9] cli_3.6.1         sass_0.4.7        renv_1.0.0        jquerylib_0.1.4  
    [13] compiler_4.3.1    rprojroot_2.0.3   rstudioapi_0.15.0 tools_4.3.1      
    [17] evaluate_0.21     bslib_0.5.0       yaml_2.3.7        jsonlite_1.8.7   
    [21] rlang_1.1.1      

------------------------------------------------------------------------

# Cite R packages used

``` r
library(grateful)
pkgs <- cite_packages(output = "table", include.RStudio = TRUE, out.dir = ".", 
                      bib.file = "ReproducibleAnalysisRGit", omit = NULL)
knitr::kable(pkgs)
```

| Package   | Version | Citation                                                                                      |
|:----------|:--------|:----------------------------------------------------------------------------------------------|
| base      | 4.3.1   | R Core Team (2023)                                                                            |
| grateful  | 0.2.0   | Francisco Rodríguez-Sánchez, Connor P. Jackson, and Shaurita D. Hutchins (2023)               |
| knitr     | 1.43    | Xie (2014); Xie (2015); Xie (2023)                                                            |
| renv      | 1.0.0   | Ushey and Wickham (2023)                                                                      |
| rmarkdown | 2.23    | Xie, Allaire, and Grolemund (2018); Xie, Dervieux, and Riederer (2020); Allaire et al. (2023) |
| rrtools   | 0.1.5   | Marwick (2019)                                                                                |
| usethis   | 2.2.2   | Wickham et al. (2023)                                                                         |

## References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-rmarkdown2023" class="csl-entry">

Allaire, JJ, Yihui Xie, Christophe Dervieux, Jonathan McPherson, Javier
Luraschi, Kevin Ushey, Aron Atkins, et al. 2023.
*<span class="nocase">rmarkdown</span>: Dynamic Documents for r*.
<https://github.com/rstudio/rmarkdown>.

</div>

<div id="ref-grateful" class="csl-entry">

Francisco Rodríguez-Sánchez, Connor P. Jackson, and Shaurita D.
Hutchins. 2023. *<span class="nocase">grateful</span>: Facilitate
Citation of r Packages*. <https://github.com/Pakillo/grateful>.

</div>

<div id="ref-rrtools" class="csl-entry">

Marwick, Ben. 2019. *<span class="nocase">rrtools</span>: Creates a
Reproducible Research Compendium*.
<https://github.com/benmarwick/rrtools>.

</div>

<div id="ref-base" class="csl-entry">

R Core Team. 2023. *R: A Language and Environment for Statistical
Computing*. Vienna, Austria: R Foundation for Statistical Computing.
<https://www.R-project.org/>.

</div>

<div id="ref-renv" class="csl-entry">

Ushey, Kevin, and Hadley Wickham. 2023.
*<span class="nocase">renv</span>: Project Environments*.
<https://CRAN.R-project.org/package=renv>.

</div>

<div id="ref-usethis" class="csl-entry">

Wickham, Hadley, Jennifer Bryan, Malcolm Barrett, and Andy Teucher.
2023. *<span class="nocase">usethis</span>: Automate Package and Project
Setup*. <https://CRAN.R-project.org/package=usethis>.

</div>

<div id="ref-knitr2014" class="csl-entry">

Xie, Yihui. 2014. “<span class="nocase">knitr</span>: A Comprehensive
Tool for Reproducible Research in R.” In *Implementing Reproducible
Computational Research*, edited by Victoria Stodden, Friedrich Leisch,
and Roger D. Peng. Chapman; Hall/CRC.

</div>

<div id="ref-knitr2015" class="csl-entry">

———. 2015. *Dynamic Documents with R and Knitr*. 2nd ed. Boca Raton,
Florida: Chapman; Hall/CRC. <https://yihui.org/knitr/>.

</div>

<div id="ref-knitr2023" class="csl-entry">

———. 2023. *<span class="nocase">knitr</span>: A General-Purpose Package
for Dynamic Report Generation in r*. <https://yihui.org/knitr/>.

</div>

<div id="ref-rmarkdown2018" class="csl-entry">

Xie, Yihui, J. J. Allaire, and Garrett Grolemund. 2018. *R Markdown: The
Definitive Guide*. Boca Raton, Florida: Chapman; Hall/CRC.
<https://bookdown.org/yihui/rmarkdown>.

</div>

<div id="ref-rmarkdown2020" class="csl-entry">

Xie, Yihui, Christophe Dervieux, and Emily Riederer. 2020. *R Markdown
Cookbook*. Boca Raton, Florida: Chapman; Hall/CRC.
<https://bookdown.org/yihui/rmarkdown-cookbook>.

</div>

</div>
